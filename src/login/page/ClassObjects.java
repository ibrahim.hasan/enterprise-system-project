package login.page;

/***********************************************************************
 
 * Author: Ismail Alson Ali Rasheed (S2000811)
 * Date Created: 01.12.2021
 * Date Last Modified: 
 *
 *  Interface Name: ClassObjects
 *  Description: Stores objects instantiated from classes which brings
 *               about the creation and modification of user details, 
 *               balances and transactions.
 
***********************************************************************/

public interface ClassObjects {
    
    ValidateDetails validateDetails = new ValidateDetails();
    ModifyDetails modifyDetails = new ModifyDetails();

}

/***********************************************************************/