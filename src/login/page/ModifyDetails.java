package login.page;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;
import javax.swing.JOptionPane;


/*********************************************************************
 
 * Author: Ismail Alson Ali Rasheed (S2000811)
 * Date Created: 01.12.2021
 * Date Last Modified: 
 *
 *  Class Name: ModifyDetails
 *  Inherits the class: RetrieveInformation
 *  Description: Deals with adding, updating and deletion of user 
 *               details stored in text files.
 
*********************************************************************/

public class ModifyDetails extends RetrieveInformation {

    RetrieveInformation retrieveInformation = new RetrieveInformation();
    
/**********************************************************************
 * Function: addUserDetails()
 * Description: Adds the details of the user entered into Registration
 *              window into the AccountDetails.txt file.
     * @param accountDetailsPath
     * @param firstName
     * @param lastName
     * @param emailAddress
     * @param contactNum
     * @param password
**********************************************************************/
    
     public void addUserDetails(String accountDetailsPath, String firstName, String lastName,
            String emailAddress, int contactNum, String password)
        {
            PrintWriter pw = null;
            
        try 
        {
            pw = new PrintWriter(new FileWriter(accountDetailsPath));
            
            pw.println(firstName + "," + lastName + "," + emailAddress 
            + "," + contactNum + "," + password);
        } 

        catch (FileNotFoundException e){
            System.out.println("Error - File Not Found in method addUserDetails()");
            e.printStackTrace();  
        } 

        catch (IOException e){
            System.out.println("Error in method addUserDetails()");
            e.printStackTrace();
        }
    
        finally
        {
            //close print writer
            pw.flush();
            pw.close();
        }
    }
    

    
/*******************************************************************************
 * Function: changeAccountDetailsRecord()
 * Description: Changes a specific record/s stored in AccountDetails.txt.
     * @param accountDetailsPath - Path of the file containing the record to be 
     *                   changed.
     * @param lastAccessedUserPath - Stores the path to LastAccessedUser.txt file
     *                               containing bankID of last logged in user.
     * @param changeRecord - The record to be changed.
     * @param newRecord - The new record.
********************************************************************************/
    
    public void changeAccountDetailsRecord(String accountDetailsPath, String lastAccessedUserPath,
            String changeRecord, String newRecord)
    {
        File file = new File(accountDetailsPath);
        
        Scanner in = null;
        PrintWriter pw = null;
        
        ArrayList<String> userDetails = new ArrayList<>();
        ArrayList<String> newUserDetails = new ArrayList<>();

        String currentLine = null;
                
        try
        {           
            String bankID;
            
            //read and add file contents to userDetails arraylist
            in = new Scanner(new FileReader(file));
            
            while(in.hasNext())
            {
                userDetails.add(in.next());
            }
            in.close();
            
            //change content of array and place into new arraylist 
            for (String data : userDetails)
            {
                currentLine = data;
                
                String[] contents = currentLine.split(",");
                
                //get bankID of current logged in user
                bankID = retrieveInformation.getBankID(lastAccessedUserPath);

                /*
                 * If the bankID read from file is equal to bankID of current user,
                 * change detail 
                */
                if(contents[7].equals(bankID))
                {
                    //variables to store all split contents of the line
                    String firstName = contents[0];
                    String lastName = contents[1];
                    String username = contents[2];
                    String emailAddress = contents[3];
                    String primaryNum = contents[4];
                    String secondaryNum = contents[5];
                    String password = contents[6];
                    String curAccNum = contents[8];
                    String savAccNum = contents[9];

                    switch(changeRecord)
                    {
                        case "First Name":
                            currentLine = newRecord + "," + lastName + "," + username + "," + emailAddress + 
                                "," + primaryNum + "," + secondaryNum + "," + password + "," + bankID + "," + curAccNum + "," + savAccNum; 
                            break;

                        case "Last Name":
                            currentLine = firstName + "," + newRecord + "," + username + "," + emailAddress + 
                                "," + primaryNum + "," + secondaryNum + "," + password + "," + bankID + "," + curAccNum + "," + savAccNum; 
                            break;

                        case "Email Address":
                            currentLine = firstName + "," + lastName + "," + username + "," + newRecord + "," +
                                primaryNum + "," + secondaryNum + "," + password + "," + bankID + "," + curAccNum + "," + savAccNum; 
                            break;

                        case "Primary Number":
                            currentLine = firstName + "," + lastName + "," + username + "," + emailAddress + 
                                "," + newRecord + "," + secondaryNum + "," + password + "," + bankID + "," + curAccNum + "," + savAccNum; 
                            break;

                        case "Secondary Number":
                            currentLine = firstName + "," + lastName + "," + username + "," + emailAddress + 
                                "," + primaryNum + "," + newRecord + "," + password + "," + bankID + "," + curAccNum + "," + savAccNum; 
                            break;

                        case "Current Account Number":
                            currentLine = firstName + "," + lastName + "," + username + "," + emailAddress +
                                    "," + primaryNum + "," + secondaryNum + "," + password + "," + bankID + "," + newRecord + "," + savAccNum; 
                            break;

                        case "Savings Account Number":
                            currentLine = firstName + "," + lastName + "," + username + "," + emailAddress + 
                                    "," + primaryNum + "," + secondaryNum + "," + password + "," + bankID + "," + curAccNum + "," + newRecord; 
                            break;

                        default:
                            break;
                    }
                }
                
                //add the modified details to newUserDetails arraylist
                newUserDetails.add(currentLine);
            }
            
            //write data in newUserDetails back into file
            pw = new PrintWriter(new BufferedWriter(new FileWriter(file)));
            
            for (String data: newUserDetails)
            {
                pw.write(data + System.lineSeparator());
            }
            pw.close();
            
            
            switch (changeRecord) 
            {
                case "First Name":
                    JOptionPane.showMessageDialog(null, "First Name Has Been Changed Successfully."
                            + "\nPlease Log Back In Again", "USER DETAIL CHANGED", JOptionPane.INFORMATION_MESSAGE);
                    break;
                    
                case "Last Name":
                JOptionPane.showMessageDialog(null, "Last Name Has Been Changed Successfully."
                        + "\nPlease Log Back In Again", "USER DETAIL CHANGED", JOptionPane.INFORMATION_MESSAGE);
                    break;
                
                case "Email Address":
                JOptionPane.showMessageDialog(null, "Email Address Has Been Changed Successfully."
                        + "\nPlease Log Back In Again", "USER DETAIL CHANGED", JOptionPane.INFORMATION_MESSAGE);
                    break;
                
                case "Primary Number":
                JOptionPane.showMessageDialog(null, "Primary Number Has Been Changed Successfully."
                        + "\nPlease Log Back In Again", "USER DETAIL CHANGED", JOptionPane.INFORMATION_MESSAGE);
                    break;
                
                case "Secondary Number":
                JOptionPane.showMessageDialog(null, "Secondary Number Has Been Changed Successfully."
                        + "\nPlease Log Back In Again", "USER DETAIL CHANGED", JOptionPane.INFORMATION_MESSAGE);
                    break;
                 
                
                case "Current Account Number":
                    JOptionPane.showMessageDialog(null, "Current Account Has Been Created Successfully. "
                            + "\nPlease Log Back In Again", "NEW ACCOUNT CREATED", JOptionPane.INFORMATION_MESSAGE);
                    break;
                    
                case "Savings Account Number":
                    JOptionPane.showMessageDialog(null, "Savings Account Has Been Created Successfully. "
                            + "\nPlease Log Back In Again", "NEW ACCOUNT CREATED", JOptionPane.INFORMATION_MESSAGE);
                    break;
                    
                default:
                    break;  
            }
        }
        
        catch(IOException e)
        {
            System.out.println("Error occured in method changeAccountDetailsRecord()");
        }
    }
    
    
    
/*********************************************************************************
 * Function: closeBankAccount()
 * Description: Permanently deletes all of the user's details and balances.
     * @param filePath - Path of the file called for. 
     * @param lastAccessedUserPath - Stores the path to LastAccessedUser.txt file
     *                               containing bankID of last logged in user.
     * @param terminateAccount - Boolean deciding whether to process with deletion.
**********************************************************************************/
    
    public void closeBankAccount(String filePath, String lastAccessedUserPath, boolean terminateAccount){
        
        String accountDetailsPath = "AccountDetails.txt";
        String accountBalancePath = "AccountBalance.txt";

        if (terminateAccount = true)
        {
            if (filePath.equals(accountDetailsPath))
            {
                File file = new File(accountDetailsPath);

                Scanner in = null;
                PrintWriter pw = null;

                ArrayList<String> userDetails = new ArrayList<>();
                ArrayList<String> newUserDetails = new ArrayList<>();

                String currentLine = null;

                try
                {           
                    String bankID;

                    //read and add file contents to userDetails arraylist
                    in = new Scanner(new FileReader(file));

                    while(in.hasNext())
                    {
                        userDetails.add(in.next());
                    }
                    in.close();

                    //change content of array and place into new arraylist 
                    for (String data : userDetails)
                    {
                        currentLine = data;

                        String[] contents = currentLine.split(",");

                        //get bankID of current logged in user
                        bankID = retrieveInformation.getBankID(lastAccessedUserPath);

                        /*
                         * If the bankID read from file is equal to bankID of current user,
                         * change detail 
                        */
                        if(contents[7].equals(bankID))
                        {
                            //do not add the current user data
                        }
                        else
                        {
                            //add details of other users to newUserDetails arraylist
                            newUserDetails.add(currentLine);
                        }

                    }

                    //write data in newUserDetails back into file
                    pw = new PrintWriter(new BufferedWriter(new FileWriter(file)));

                    for (String data: newUserDetails)
                    {
                        pw.write(data + System.lineSeparator());
                    }
                    pw.close();


                    JOptionPane.showMessageDialog(null, "Your Bank Account Has Been Closed Successfully"
                          ,"BANK ACCOUNT CLOSED", JOptionPane.INFORMATION_MESSAGE);
                }

                catch(IOException e)
                {
                    System.out.println("Error occured in method closeBankAccount() "
                                       + "of filePath: AccountDetails.txt");
                }       

            }

            else if (filePath.equals(accountBalancePath))
            {
                File file = new File(accountBalancePath);

                Scanner in = null;
                PrintWriter pw = null;

                ArrayList<String> userDetails = new ArrayList<>();
                ArrayList<String> newUserDetails = new ArrayList<>();

                String currentLine = null;

                try
                {           
                    String bankID;

                    //read and add file contents to userDetails arraylist
                    in = new Scanner(new FileReader(file));

                    while(in.hasNext())
                    {
                        userDetails.add(in.next());
                    }
                    in.close();

                    //change content of array and place into new arraylist 
                    for (String data : userDetails)
                    {
                        currentLine = data;

                        String[] contents = currentLine.split(",");

                        //get bankID of current logged in user
                        bankID = retrieveInformation.getBankID(lastAccessedUserPath);

                        /*
                         * If the bankID read from file is equal to bankID of current user,
                         * change detail 
                        */
                        if(contents[0].equals(bankID))
                        {
                            //do not add the current user data
                        }
                        else
                        {
                            //add details of other users to newUserDetails arraylist
                            newUserDetails.add(currentLine);
                        }
                    }

                    //write data in newUserDetails back into file
                    pw = new PrintWriter(new BufferedWriter(new FileWriter(file)));

                    for (String data: newUserDetails)
                    {
                        pw.write(data + System.lineSeparator());
                    }
                    pw.close();
                }

                catch(IOException e)
                {
                    System.out.println("Error occured in method closeBankAccount() "
                                        + "of filePath: AccountBalance.txt");
                }
            }
        }
    }
    
} //END OF CLASS//
