package login.page;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

/***************************************************************************
 
 * Author: Ismail Alson Ali Rasheed (S2000811)
 * Date Created: 01.12.2021
 * Date Last Modified: 
 *
 *  Class Name: RetrieveInformation
 *  Inherits the class: 
 *  Description: Contains methods that deals with the retrieval and display 
 *               of data stored in text files that includes the user details
 *               and balances.
 
***************************************************************************/

public class RetrieveInformation {
    
    
/**********************************************************************
 * Function: getBankID()
 * Description: Gets the bankID of the user currently using the 
 *              application from the LastAccessedUser.txt file.
     * @param lastAccessedUserPath - Path of file "LastAccessedUser.txt"
     *                        which stores the bankID of last login user.
     * @return bankID
**********************************************************************/ 
    
    public String getBankID(String lastAccessedUserPath) {
        
        Scanner in = null;
        String bankID = null;
        
        try 
        {
            File file = new File(lastAccessedUserPath);
            in = new Scanner(file);

            while (in.hasNextLine()) 
            {
                bankID = in.nextLine();
                return bankID;
            }
        } 
        
        catch(IOException e)
        {
            System.out.println("Error occured in method getBankID()");
        }
        
        finally
        {
            try
            {
                in.close();
            }
            catch (Exception e)
            {
                System.out.println("Error occured in closing scanner of the method getBankID()");
            }
        }
        
        return bankID; 
    }
    
    

/********************************************************************************
 * Function: getUserDetail()
 * Description: Gets a specific detail of the user stored in the
 *              AccountDetails.txt file.
    * @param accountDetailsPath - Path of the file with user details.
    * @param lastAccessedUserPath - Stores the path to LastAccessedUser.txt file
    *                               containing bankID of last logged in user.
    * @param detailIndex - The position of the array which corresponds 
    *                      to a specific detail of the user which is called for:
    *                           * detailIndex = 0 ---> First Name
                                * detailIndex = 1 ---> Last Name
                                * detailIndex = 2 ---> Username
                                * detailIndex = 3 ---> Email Address
                                * detailIndex = 4 ---> Contact Number (primary)
                                * detailIndex = 5 ---> Contact Number (secondary)
                                * detailIndex = 8 ---> Current Account Number
                                * detailIndex = 9 ---> Savings Account Number
    * @return userDetail - The detail of the user called.
*********************************************************************************/
    
    public String getUserDetail(String accountDetailsPath, String lastAccessedUserPath, int detailIndex){
        
        String delimeter = ",";
        String line = "";
        
        BufferedReader br = null;
        String userDetail = null;
        
        try 
        {
            //get bankID of the current logged in user 
            String bankID = getBankID(lastAccessedUserPath); 
            
            //creating buffered reader object
            br = new BufferedReader(new FileReader(accountDetailsPath));
            
            //read line and loop until no line is found
            while((line = br.readLine()) != null){
                
                String[] file;
                file = line.split(delimeter);

                /** 
                 * If bankID is equal to 0th index, get the user detail called for
                 * from the detailIndex.
                **/
                if (file[7].equals(bankID)){
                    userDetail = file[detailIndex];
                }
            }
        } 
        
        catch (Exception e) {
            System.out.println("Error occured in method getUserDetail()");
        }
        
        finally
        {
            try 
            {
                br.close();
            } 
            catch (IOException e) {
                System.out.println("Error occured in closing reader in method getUserDetail()!");
                e.printStackTrace();
            }
        }
        
        return userDetail;
    }
    
    

    
/*********************************************************************************
 * Function: getAccountBalance()
 * Description: Retrieves the balance of the account called for from the 
 *              text file.
    * @param accountBalancePath - Path of the file with user balance/s.
    * @param lastAccessedUserPath - Stores the path to LastAccessedUser.txt file
    *                               containing bankID of last logged in user.
    * @param balanceIndex - The position of the array which corresponds 
    *                       to the account type and it's balance:
                                * accountIndex = 2 ---> Current Account Balance
                                * accountIndex = 4 ---> Savings Account Balance
    * @return accountBalance - Balance of the account called.
*********************************************************************************/
    
    public Double getBalance(String accountBalancePath, String lastAccessedUserPath, int balanceIndex)
    {
        BufferedReader br = null;
        String line = ""; 
        String delimeter = ",";

        Double accountBalance = null;
        
        try 
        {
            //get bankID of the current user 
            String bankID = getBankID(lastAccessedUserPath);
            
            br = new BufferedReader(new FileReader(accountBalancePath));
            
            while((line = br.readLine()) != null){
                
                String[] file;
                file = line.split(delimeter);

                /** 
                 * If bankID is equal to 0th index, get the oldBalance from the
                 * accountIndex and parse it into double format.
                **/
                if (file[0].equals(bankID)){
                    accountBalance = Double.parseDouble(file[balanceIndex]);
                    return accountBalance;
                }
            }
            //System.out.println("Balance is: " +accountBalance);
        } 
        
        catch (Exception e) 
        {
            //System.out.println("Error occured in method getBalance()");
        }
        
        finally
        {
            try 
            {
                br.close();
            } 
            catch (IOException e) {
                System.out.println("Error occured in closing reader in method getBalance()");
                e.printStackTrace();
            }
        }    
        
        
        if (accountBalance == null){
            //System.out.println("Account Does Not Exist!");
            return null;
        }
        else{
            return accountBalance;
        }
    }


/*********************************************************************************
 * Function: showBalance()
 * Description: Shows the balance of the account called for from the 
 *              text file in the panel.
    * @param accountBalancePath - Path of the file with user balance/s.
    * @param lastAccessedUserPath - Stores the path to LastAccessedUser.txt file
    *                               containing bankID of last logged in user.
    * @param balanceIndex - The position of the array which corresponds 
    *                       to the account type and it's balance:
    *                           * accountIndex = 2 ---> Current Account Balance
                                * accountIndex = 4 ---> Savings Account Balance
    * @param jPanel - Panel on MainMenu which displays the account balance.
    * @param jLabel - Label on MainMenu which displays the account balance.

*********************************************************************************/
    
    public void showBalance(String accountBalancePath, String lastAccessedUserPath, int balanceIndex, 
                       javax.swing.JPanel jPanel,javax.swing.JLabel jLabel)
    {
        Double accountBalance = getBalance(accountBalancePath, lastAccessedUserPath, balanceIndex);
        
        if (accountBalance != null)
        {
            jPanel.setVisible(true);

            //set the text in panel to the balance
            jLabel.setText(Double.toString(accountBalance));
        }
        else
        {
            jPanel.setVisible(false);
        }
    }
}
